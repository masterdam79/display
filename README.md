# Display

This repository is meant to provide a very simple solution.

If installed on a webserver, you will be able to simply access the url if no GET parameter is passed, there will be a text field which posts to the GET variable and if present displays that text in the middle of the screen.
