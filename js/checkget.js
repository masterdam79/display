$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
                      .exec(window.location.search);

    return (results !== null) ? results[1] || 0 : false;
}

$(function() {
  if ( $.urlParam('text') ) {
    text = $.urlParam('text');
    var replaced = text.replace(/\+/g, "%20");
    var stripped = decodeURIComponent(replaced)
    //console.log('query string parameter passed, showing text');
    $( "div#innerhtml" ).replaceWith( "<h1 class=\"gettext text-justify display-1\">" + stripped + "</h1>" );
  } else {
    //console.log('no query string parameter passed, giving form');
    $( "div#innerhtml" ).replaceWith( "<h1 class=\"display-1\">Fill in the text you'd like to show on the screen</h1><br/><form style=\"width: 100%;\"><div class=\"form-group\"><input type=\"text\" name=\"text\" class=\"form-control form-control-lg\" id=\"text\" placeholder=\"Text to display\" required></div><button type=\"submit\" class=\"btn btn-info btn-primary\">Submit</button></form>" );
  };
});
